$(document).ready(function(){
	var mainNav = $('.my-main-nav'),
		mainNavTopPosition = mainNav.offset().top,
		addressOffesetTop = $('#my-contact-address').offset().top + $('#my-contact-address').height() + parseInt($('#my-contact-address').css('paddingTop').replace('px', '')),
		contentSections = $('.my-section');
	
	$(window).on('scroll', function(){
		//on desktop - assign a position fixed to logo and action button and move them outside the viewport
		( $(window).scrollTop() > addressOffesetTop ) ? $('#my-logo, .my-btn').addClass('is-hidden') : $('#my-logo, .my-btn').removeClass('is-hidden');
		
		//on desktop - fix main navigation on scrolling
		if($(window).scrollTop() > mainNavTopPosition ) {
			//fix main navigation
			mainNav.addClass('is-fixed');
			//push the .my-main-content giving it a top-margin
			$('.my-main-content').addClass('has-top-margin');	
			setTimeout(function() {
					mainNav.addClass('animate-children');
					$('#my-logo').addClass('slide-in');
					$('#my-logo img').attr('src', '/img/northland.png');
					$('.my-btn').addClass('slide-in');

	        }, 50);
		} else {
			mainNav.removeClass('is-fixed');
			$('.my-main-content').removeClass('has-top-margin');
			setTimeout(function() {
	            mainNav.removeClass('animate-children');
							$('#my-logo').removeClass('slide-in');
							$('#my-logo img').attr('src', '/img/white-logo.png');
				$('.my-btn').removeClass('slide-in');
	        }, 50);
		}
 

		//on desktop - update the active link in the main fixed navigation
		updatemainNavigation();
	});

	function updatemainNavigation() {
		contentSections.each(function(){
			var actual = $(this),
				actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
				actualAnchor = mainNav.find('a[href="#'+actual.attr('id')+'"]');
			if ( ( actual.offset().top - mainNav.height() <= $(window).scrollTop() ) && ( actual.offset().top +  actualHeight - mainNav.height() > $(window).scrollTop() ) ) {
				actualAnchor.addClass('active');
			}else {
				actualAnchor.removeClass('active');
			}
		});
	} 

	//on mobile - open/close main navigation clicking/tapping the .my-main-nav-trigger
	$('.my-main-nav-trigger').on('click', function(event){
		event.preventDefault();
		$(this).toggleClass('menu-is-open');
		mainNav.find('ul').toggleClass('is-visible');
	});


	mainNav.find('ul a').on('click', function(event){
        event.preventDefault();
        var target= $(this.hash);
        $('body,html').animate({
        	'scrollTop': target.offset().top - (mainNav.height()*1.5)
        	}, 400
				); 
				
        //on mobile - close main navigation
        $('.my-main-nav-trigger').removeClass('menu-is-open');
        mainNav.find('ul').removeClass('is-visible');
    });


});

