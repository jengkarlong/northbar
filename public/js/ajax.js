/**
 * 
 * AJAX FOR NEWS & EVENTS
 * 
 */
$('.read-more').on('click', function () {
  var id = $(this).data('index');
  $.ajax({
    method: "GET",
    url: "/news/show",
    data: {
      id: id
    },
    beforeSend: function() {
      $('#loader').css('display', 'block');
    },
    complete: function() {
      setTimeout(function() {
        $('#news-overlay').addClass('yes-show')
        $('#loader').css('display', 'none');
        
      }, 1000);
    }, 
    success: function(data) {
      $('#news-overlay').html(data);
      $('body').css('overflow', 'hidden');

      $('.close-news').on('click', function() {
        $('#news-overlay').removeClass('yes-show');
        $('body').css('overflow', 'visible');
      });
    }
  });
});


$('.view-event').on('click', function () {
  var id = $(this).data('index');
  $.ajax({
    method: "GET",
    url: "/events/show",
    data: {
      id: id
    },
    beforeSend: function() {
      $('#loader').css('display', 'block');
    },
    complete: function() {
      setTimeout(function() {
        $('#news-overlay').addClass('yes-show')
        $('#loader').css('display', 'none');
        
      }, 1000);
    }, 
    success: function(data) {
      $('#news-overlay').html(data);
      $('body').css('overflow', 'hidden');

      $('.close-news').on('click', function() {
        $('#news-overlay').removeClass('yes-show');
        $('body').css('overflow', 'visible');
      });
    }
  });
});



/**
 * 
 * AJAX FOR MENUS
 * 
 */


$('.menu, .menu-btn').on('mouseup', function(){
  var url = $(this).data('url');
  $.ajax({
    method: "GET",
    url: "menus/show",
    data: {
      menu: url
    },
    beforeSend: function() {
      $('#loader').css('display', 'block');
      checkURL(url);
    },
    complete: function() {
      setTimeout(function() {
        $('#menu-overlay').css('display', 'block')
        $('#loader').css('display', 'none');
        
      }, 1000);
    },
    success: function(data) {
      $('.menu-container').html(data);
      $('body').css('overflow', 'hidden');

      $('.close-news').on('click', function() {
        $('#menu-overlay').css('display','none');
        $('body').css('overflow', 'visible');
      });
    }
  }) 
})

//left & right button data and icon
function checkURL(url)
{
  if (url == 'food') {
    $('.menu-btn').attr('id','');
    $('#menu-overlay').css('background-image', 'url("/img/menu-bg.jpeg"')    
    $('.drink-btn').attr('id', "left-btn")
    $('.cocktail-btn').attr('id', "right-btn")
  }else if (url == 'drinks') {
    $('.menu-btn').attr('id','');
    $('#menu-overlay').css('background-image', 'url("/img/backpic.jpeg"')
    $('.cocktail-btn').attr('id', "left-btn")
    $('.food-btn').attr('id', "right-btn")
  }else if (url == 'cocktails') {
    $('.menu-btn').attr('id','');
    $('#menu-overlay').css('background-image', 'url("/img/cocktail-bg.jpeg"')
    $('.food-btn').attr('id', "left-btn")
    $('.drink-btn').attr('id', "right-btn")
  }
}