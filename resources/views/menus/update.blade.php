
<div class="field">
    <label for="category"><span class="has-text-danger">*</span> Category</label>
    <div class="control">
      <div class="select is-info">
        <select  name="category" id="category">
          <option value="{{$menu->category}}">{{$menu->category}}</option>
          <option value="food">food</option>
          <option value="drinks">drinks</option>
          <option value="cocktails">cocktails</option>
        </select>
      </div>
    </div>
</div>

  <div class="field">
    <label for="subcategory"><span class="has-text-danger">*</span> Subcategory</label>
    <div class="control">
      <input class="input is-info" type="text" name="subcategory" id="subcategory" value="{{$menu->subcategory}}">
    </div>
  </div>
  
  <div class="field">
      <label for="name"><span class="has-text-danger">*</span> Name</label>
      <div class="control">

        <input class="input is-info" type="text" name="name" id="name" value="{{$menu->name}}">
      </div>
  </div>
  

  <div class="field">
    <label for="price"><span class="has-text-danger">*</span> Price</label>
    <div class="control">

      <input class="input is-info" type="number" name="price" id="price" step="any" value="{{$menu->price}}">
    </div>
  </div>

  <div class="field">
    <label for="description">Description</label>
    <div class="control">

      <input class="input is-info" type="text" name="description" id="description" maxlength="150" value="{{$menu->description}}">
    </div>
    <div class="help is-info">Min char: 0 | Max char: 150</div>
  </div>

    
<button class="button is-primary" id="update-menu" data-index="{{$menu->id}}">Save</button>
<button class="button modal-cancel">Cancel</button>



<script>
      $('#update-menu').on('click', function() {
      var id = $(this).data('index'),
          category = $('#category').val(),
          subcategory = $('#subcategory').val(),
          name = $('#name').val(),
          price = $('#price').val(),
          description = $('#description').val();

          
      $.ajax({
        method: "POST",
        url: "/menus/update",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          id: id,
          category: category,
          subcategory: subcategory,
          name: name,
          price: price,
          description: description
        }, 
        success: function(data) {
          console.log(data);
          location.reload();
        } 
      });
    });
</script>