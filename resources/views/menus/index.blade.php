@extends ('admin.master')


@section ('content')
<a href="/menus/create" class="button is-info is-outlined">Create New</a>

<h1 class="is-size-2">Menus</h1>

<table id="menu" class="table is-striped is-bordered is-hoverable is-fullwidth">
    <thead>
      <th>Category</th>
      <th>Subcategory</th>      
      <th>Name</th>
      <th>Price</th>
      <th>Updated at</th>
      <th></th>
    </thead>
    <tbody>
      @foreach ($menus as $menu)
        <tr>
          <td>{{$menu->category}}</td>
          <td>{{$menu->subcategory}}</td>
          <td>{{$menu->name}}</td>
          <td>${{$menu->price}}</td>
          <td>{{$menu->updated_at}}</td>
          <td>
            <button class="button is-danger delete-menu" data-index="{{$menu->id}}">Delete</button>
            <button class="button is-warning has-text-white edit-menu" data-index="{{$menu->id}}">Edit</button>

          </td>
        </tr>
      @endforeach
    </tbody>
  </table>

<div class="modal">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title">Edit</p>
      <button class="delete" aria-label="close"></button>
    </header>
      <section class="modal-card-body">
      

    </section>
   
  </div>
</div>

<script>
    $('.delete-menu').on('click', function() {
      var id = $(this).data('index');
  
      $.ajax({
        method: 'POST',
        url: 'menus/destroy',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          id: id
        },
        success: function(data) {
          location.reload();  
        }
      })
    });
    $('.edit-menu').on('click', function() {
      var id = $(this).data('index');

      $.ajax({
        method: "GET",
        url: "/menus/update",
        data: {
          id: id
        },
        success: function(data) {
          $('.modal-card-body').html(data);
          $('.modal').addClass('is-active');
          $('.modal .delete, .modal-cancel, #update-menu').on('click', function() {
            $('.modal').removeClass('is-active');
          });
        }
      })
    });


</script>


@endsection
