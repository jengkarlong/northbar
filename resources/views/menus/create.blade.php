@extends ('admin.master')



@section ('content')
<a href="/menus" class="button is-primary is-outlined">Back to Menus</a>

<h1 class="is-size-2">Create Menu</h1>

<form action="/menus/store" method="POST" enctype="multipart/form-data">
  @csrf

  <div class="field">
    <label for="category"><span class="has-text-danger">*</span> Category</label>
    <div class="control">
      @if ($e = $errors->first('category'))
      <div class="select is-danger">
        <select  name="category" id="category">
          <option value="#" selected disabled></option>
          <option value="food">Food</option>
          <option value="drinks">Drinks</option>
          <option value="cocktails">Cocktails</option>
        </select>
      </div>
      </div>
      <div class="help is-danger">{{$e}}</div>
      @else
      <div class="select is-info">
        <select  name="category" id="category">
          <option value="#" selected disabled></option>
          <option value="food">Food</option>
          <option value="drinks">Drinks</option>
          <option value="cocktails">Cocktails</option>
        </select>
      </div>
      </div>
      @endif
    
  </div>

  <div class="field">
    <label for="subcategory"><span class="has-text-danger">*</span> Subcategory</label>
    <div class="control">
    @if ($e = $errors->first('subcategory')) 
      <input class="input is-danger" type="text" name="subcategory" id="subcategory">
    </div>
      <div class="help is-danger">{{$e}}</div>
    @else
      <input class="input is-info" type="text" name="subcategory" id="subcategory">
    </div>
    @endif
  </div>
  
  <div class="field">
      <label for="name"><span class="has-text-danger">*</span> Name</label>
      <div class="control">
      @if ($e = $errors->first('name')) 
        <input class="input is-danger" type="text" name="name" id="name">
      </div>
        <div class="help is-danger">{{$e}}</div>
      @else
        <input class="input is-info" type="text" name="name" id="name">
      </div>
      @endif
  </div>
  

  <div class="field">
    <label for="price"><span class="has-text-danger">*</span> Price</label>
    <div class="control">
    @if ($e = $errors->first('price')) 
      <input class="input is-danger" type="number" name="price" id="price" step="any">
    </div>
    <div class="help is-danger">{{$e}}</div>
    @else
      <input class="input is-info" type="number" name="price" id="price" step="any">
    </div>
    @endif
  </div>

  <div class="field">
    <label for="description">Description</label>
    <div class="control">
    @if ($e = $errors->first('description')) 
      <input class="input is-danger" type="text" name="description" id="description" maxlength="150">
    </div>
    <div class="help is-danger">{{$e}}</div>
    @else
      <input class="input is-info" type="text" name="description" id="description" maxlength="150">
    </div>
    <div class="help is-info">Min char: 0 | Max char: 150</div>
    @endif
  </div>
  

  
  <div class="control">
      <button type="submit" class="button is-primary">Submit</button>
  </div>
</form>

@endsection

