@inject('menu', 'App\Menu')

<div class="close-news"></div>
<h2 class="category">{{$category}}</h2>
@foreach ($sub as $data)
<div class="subcategory">
  @php $new = $menu->where('subcategory', $data->subcategory)->get() @endphp
  <h3>{{$data->subcategory}}</h3>
  <div class="grid">
  @foreach ($new as $item)
  <div class="item">
    <p class="title" >{{$item->name}}</p>
    <p class="description">{{$item->description}}</p>
    <p class="price">${{$item->price}}</p>
  </div>
  
  @endforeach
  </div>
</div>
@endforeach



<script>
 $('.grid').each(function(i) {
   console.log($('.grid').eq(i).children('.item').length);
 });

</script>