<!-- Will be called by ajax -->
<div class="news-bg" style="background-image: url('/img/news/{{$news->image}}" alt="News Image" id="a-news-img"></div>
<div class="news-overlay">
  <div class="close-news"></div>
  <h2 id="a-news-title">{{$news->title}}</h2>
  <p id="a-news-body">{{$news->body}}</p>
  <img src="/img/news/{{$news->image}}" alt="News Image" id="a-news-img">
</div>

