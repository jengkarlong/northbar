@extends ('admin.master')


@section ('content')
<a href="/news/create" class="button is-info is-outlined">Create New</a>

<h1 class="is-size-2" style="margin: 30px 0">News</h1>
<table class="table is-striped is-bordered is-hoverable is-fullwidth">
  <thead>
    <th>Title</th>
    <th>Created at</th>
    <th>Updated at</th>
    <th></th>
  </thead>
  <tbody>
    @foreach ($news as $item)
      <tr>
        <td>{{$item->title}}</td>
        <td>{{$item->created_at}}</td>
        <td>{{$item->updated_at}}</td>
        <td>
          <button class="button is-danger delete-news" data-index="{{$item->id}}">Delete</button>
          <button class="button is-warning edit-news" data-index="{{$item->id}}">Edit</button>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

<div class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <p class="modal-card-title">Edit</p>
        <button class="delete" aria-label="close"></button>
      </header>
        <section class="modal-card-body">
        
  
      </section>
     
    </div>
  </div>
<script>
  $('.delete-news').on('click', function() {
    var id = $(this).data('index');

    $.ajax({
      method: 'POST',
      url: 'news/destroy',
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      data: {
        id: id
      },
      success: function(data) {
        location.reload();        
      }
    })
  });
  $('.edit-news').on('click', function() {
      var id = $(this).data('index');

      $.ajax({
        method: "GET",
        url: "/news/update",
        data: {
          id: id
        },
        success: function(data) {
          $('.modal-card-body').html(data);
          $('.modal').addClass('is-active');
          $('.modal .delete, .modal-cancel, #update-news').on('click', function() {
            $('.modal').removeClass('is-active');
          });
        }
      })
    });
</script>

@endsection

