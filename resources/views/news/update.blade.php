<div class="field">
    <label for="title"><span class="has-text-danger">*</span> Title</label>
    <div class="control">

      <input class="input is-info" type="text" name="title" id="title" value="{{$news->title}}">
    </div>
 
</div>


<div class="field">
    <label for="body"><span class="has-text-danger">*</span> Body</label>
    <div class="control">

      <input class="input is-info" type="text" name="body" id="body" value="{{$news->body}}">
    </div>
    
</div>

<div class="help is-info">To upload new image. Please add new entry and delete the old one</div>
<button class="button is-primary" id="update-news" data-index="{{$news->id}}">Save</button>
<button class="button modal-cancel">Cancel</button>



<script>
      $('#update-news').on('click', function() {
      var id = $(this).data('index'),
          title = $('#title').val(),
          body = $('#body').val();

          
      $.ajax({
        method: "POST",
        url: "/news/update",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          id: id,
          title: title,
          body: body
        }, 
        success: function(data) {
          location.reload();
        } 
      });
    });
</script>