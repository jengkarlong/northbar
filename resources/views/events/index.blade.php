@extends ('admin.master')


@section ('content')
<a href="/events/create" class="button is-info is-outlined">Create New</a>

<h1 class="is-size-2">Events</h1>
<table class="table is-striped is-bordered is-hoverable is-fullwidth">
  <thead>
    <th>Title</th>
    <th>Date</th>
    <th>Start</th>
    <th>Created at</th>
    <th></th>
  </thead>
  <tbody>
    @foreach ($events as $event)
      <tr>
        <td>{{$event->title}}</td>
        <td>{{date('D M j', strtotime($event->date))}}</td>
        <td>{{date('g:i A' ,strtotime($event->date))}}</td>
        <td>{{$event->created_at}}</td>
        <td>
          <button class="button is-danger delete-event" data-index="{{$event->id}}">Delete</button>
          <button class="button is-warning edit-event" data-index="{{$event->id}}">Edit</button>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>

<div class="modal">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <p class="modal-card-title">Edit</p>
        <button class="delete" aria-label="close"></button>
      </header>
        <section class="modal-card-body">
        
  
      </section>
     
    </div>
  </div>
<script>
    $('.delete-event').on('click', function() {
      var id = $(this).data('index');
  
      $.ajax({
        method: 'POST',
        url: 'events/destroy',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          id: id
        },
        success: function(data) {
          location.reload();        
        }
      })
    });

      $('.edit-event').on('click', function() {
      var id = $(this).data('index');

      $.ajax({
        method: "GET",
        url: "/events/update",
        data: {
          id: id
        },
        success: function(data) {
          $('.modal-card-body').html(data);
          $('.modal').addClass('is-active');
          $('.modal .delete, .modal-cancel, #update-event').on('click', function() {
            $('.modal').removeClass('is-active');
          });
        }
      })
    });
</script>

@endsection

