<!-- Will be called by ajax -->

<div class="news-bg" style="background-image: url('/img/events/{{$event->image}}" alt="News Image" id="a-news-img"></div>
<div class="news-overlay">
  <div class="close-news"></div>
  <h2 id="a-news-title">{{$event->title}}</h2>
  <p class="date" style="margin-right: 10px"><strong>Date: </strong>{{date('D M j', strtotime($event->date))}} <strong>Start: </strong>{{date('g:i A' ,strtotime($event->date))}}</p>
  <p class="time"></p>
          
  <img src="/img/events/{{$event->image}}" alt="News Image" id="a-news-img">
</div>

