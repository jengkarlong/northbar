<div class="field">
    <label for="title"><span class="has-text-danger">*</span> Title</label>
    <div class="control">
        <input class="input is-info" type="text" name="title" id="title" value="{{$event->title}}">
    </div>
  </div>


  <div class="field">
    <label for="date"><span class="has-text-danger">*</span> Date</label>
    <div class="control">
      <input class="input is-info" type="datetime-local" name="date" id="date">
    </div>
  </div>

<div class="help is-info">To upload new image. Please add new entry and delete the old one</div>
<button class="button is-primary" id="update-event" data-index="{{$event->id}}">Save</button>
<button class="button modal-cancel">Cancel</button>



<script>
      $('#update-event').on('click', function() {
      var id = $(this).data('index'),
          title = $('#title').val(),
          date = $('#date').val();

          
      $.ajax({
        method: "POST",
        url: "/events/update",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          id: id,
          title: title,
          date: date
        }, 
        success: function(data) {
          location.reload();
        } 
      });
    });
</script>