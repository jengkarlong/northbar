@extends ('admin.master')


@section ('content')
  <a href="/events" class="button is-primary is-outlined">Back to Events</a>
  <h1 class="is-size-2">Create Event</h1>
  <form action="/events/store" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="field">
      <label for="title"><span class="has-text-danger">*</span> Title</label>
      <div class="control">
          @if ($e = $errors->first('title')) 
          <input class="input is-danger" type="text" name="title" id="title">
      </div>
          <div class="help is-danger">{{$e}}</div>
          @else
          <input class="input is-info" type="text" name="title" id="title">
      </div>
          @endif
    </div>


    <div class="field">
      <label for="date"><span class="has-text-danger">*</span> Date</label>
      <div class="control">
        @if ($e = $errors->first('date')) 
        <input class="input is-danger" type="datetime-local" name="date" id="date">
      </div>
        <div class="help is-danger">{{$e}}</div>
        @else
        <input class="input is-info" type="datetime-local" name="date" id="date">
      </div>
        @endif
    </div>

    <div class="field">
        @if ($e = $errors->first('image')) 
        <div class="file is-danger has-name">
            <label class="file-label">
            <input class="file-input" type="file" name="image">
            <span class="file-cta">
                <span class="file-icon">
                    <i class="fas fa-upload"></i>
                </span>
                <span class="file-label">
                Upload Image
                </span>
            </span>
            </label>
        </div>
        <div class="help is-danger">{{$e}}</div>
        @else
            <div class="file is-info has-name">
                <label class="file-label">
                <input class="file-input" type="file" name="image">
                <span class="file-cta">
                    <span class="file-icon">
                        <i class="fas fa-upload"></i>
                    </span>
                    <span class="file-label">
                    Upload Image
                    </span>
                </span>
                </label>
            </div>
        @endif
    </div>
    <div class="control">
        <button type="submit" class="button is-primary">Submit</button>
    </div>
  </form>

@endsection

