<nav  class="navbar is-fixed-top is-info" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a href="/" class="navbar-item">
      <img src="/img/northland.png" alt="Northland Logo" width="112" height="28">
    </a>
    <div class="navbar-burger" data-target="navMenu">
        <span></span>
        <span></span>
        <span></span>
    </div>
  </div>




  <div class="navbar-menu" id="navMenu">
    <div class="navbar-end">
      @if (Auth::check())
      <a href="#" class="navbar-item">Hi, {{Auth::user()->name}}</a>
      <a href="/menus" class="navbar-item">Menus</a>
      <a href="/news" class="navbar-item">News</a>
      <a href="/events" class="navbar-item">Events</a>
      <a href="/logout" class="navbar-item">Logout</a>
      @else 
      <a href="/login" class="navbar-item">Sign-in</a>
      @endif
    </div>
  </div>

</nav>

<!-- Bulma script -->
<script>
document.addEventListener('DOMContentLoaded', function () {
// Get all "navbar-burger" elements
  var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {

    // Add a click event on each of them
    $navbarBurgers.forEach(function ($el) {
      $el.addEventListener('click', function () {

        // Get the target from the "data-target" attribute
        var target = $el.dataset.target;
        var $target = document.getElementById(target);

        // Toggle the class on both the "navbar-burger" and the "navbar-menu"
        $el.classList.toggle('is-active');
        $target.classList.toggle('is-active');

      });
    });
  }

  });
</script>