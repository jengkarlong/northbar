@extends ('admin.master')

@section ('content')

  <h1 class="is-size-2">Login</h1>

  <form action="/login" method="POST">
  @csrf

  <div class="field">
    <label for="username"><span class="has-text-danger">*</span> Username</label>
    <div class="control">
    @if ($e = $errors->first('message')) 
      <input class="input is-danger" type="text" name="username" id="username">
    </div>
    @else
      <input class="input is-info" type="text" name="username" id="username">
    </div>
    @endif
  </div>

  <div class="field">
    <label for="password"><span class="has-text-danger">*</span> Password</label>
    <div class="control">
    @if ($e = $errors->first('message')) 
      <input class="input is-danger" type="password" name="password" id="password">
      <div class="help is-danger">{{$e}}</div>
    </div>
    @else
      <input class="input is-info" type="password" name="password" id="password">
    </div>
    @endif
  </div>

    
  <div class="control">
    <button type="submit" class="button is-primary">Submit</button>
  </div>
  
  
  </form>
  

@endsection