<section id="my-placeholder-1" class="my-section my-container about">

  <div id="about-content">
    <h2>About Us</h2>
    <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam, ipsam.</h3>
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et a natus sed quidem vero quia tempora similique porro doloremque, quis dicta. Laudantium id sunt autem, natus veniam architecto quod magni incidunt. Esse alias repellendus possimus. Hic sit officia cupiditate quidem.</p>
    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ea non odit at asperiores necessitatibus molestiae commodi iure ut deserunt fuga.</p>
  </div>
  <div id="old-pic">
    <img src="/img/about-pic.jpg" alt="Northland Bar">
    <div class="white-overlay"></div>
  </div>

</section>