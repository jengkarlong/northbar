@extends ('layouts.master')

@section ('page_title') Northland Sports Bar &amp; Grill @endsection

@section ('add_head')
<!-- Leafletjs map -->
	<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
	integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
	crossorigin=""/>
	<script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
	integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
	crossorigin=""></script>
@endsection


@section ('content')

<main class="my-main-content">
	
	<!-- About -->
@include ('home.about')

	<!-- Menu -->
@include ('home.menu')
				
	<!-- Event -->
@include ('home.event')

	<!-- Location -->
@include ('home.location')

	<!-- Contact -->
@include ('home.contact')

</main> <!-- .my-main-content -->
@endsection


@section ('external_js')

	<script src="/js/main.js"></script>
	<script src="/js/ajax.js"></script>

@endsection