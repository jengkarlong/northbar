<section id="my-placeholder-4" class="my-section my-container location">
    <!-- <p>N10103 WI-49, Iola, WI 5494</p> -->
    <div id="mapid">

    </div>
</section>


    
<script>
var map,
    marker,
    pos = {lat: 44.6061087, lng: -89.2139784};

function initMap() {
    var styledMapType = new google.maps.StyledMapType ([
        {
            "stylers": [
                {
                    "hue": "#dd0d0d"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "lightness": 100
                },
                {
                    "visibility": "simplified"
                }
            ]
        }
    ],{name: 'Northland Bar'});
    map = new google.maps.Map(document.getElementById('mapid'), {
    center: pos,
    zoom: 13,
    mapTypeControlOptions: {
        mapTypeIds: ['roadmap', 'satellite', 'hybrid', 'terrain', 'styled_map']
    }
    });
    map.mapTypes.set('styled_map', styledMapType);
    map.setMapTypeId('styled_map');
    marker = new google.maps.Marker({
        position: pos,
        map: map
    });

}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6eGNudxDiOwvDeufbcoleTMG8fxbbplg&callback=initMap"
async defer></script>
