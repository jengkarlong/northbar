<section id="my-placeholder-5" class="my-section my-container contact" style="background-image: url('/img/contact.jpeg')">
  <div class="whitebg">
      <h2>Get in touch</h2>
  </div>

  <div id="contact">
    <h4><i class="fas fa-map-marker-alt"></i>N10103 WI-49 Iola, Wisconsin</h4>
    <h4><i class="fas fa-mobile-alt"></i><a href="tel: 715-677-3491">715-677-3491</a></h4>
    <h4><i class="fas fa-envelope"></i><a href="mail: info@northbar.com">info@northbar.com</a></h4>
    <h4><i class="fab fa-facebook-f"></i><a href="#">Like us on facebook</a></h4>
  </div>
</section> 