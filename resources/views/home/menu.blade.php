<section id="my-placeholder-2" class="my-section my-container menus">

  <div id="menu-container" style="background-image: url('/img/menu-bg.jpeg')"> 
    <h2>Food &amp; Drink Menus</h2>
    <div class="menu-types">
      <div class="drink menu" data-url="drinks">
        <h3>Drinks</h3>
        <span><img src="/img/mug.png" alt="Drink menu icon"></span>
      </div>
      <div class="food menu" data-url="food">
        <h3>Food</h3>
        <img src="/img/dish.png" alt="Food menu icon">
      </div>
      <div class="cocktail menu" data-url="cocktails">
        <h3>Cocktails</h3>
        <img src="/img/toast.png" alt="Cocktail menu icon">
      </div>
    </div>
  </div>

</section>

<div id="menu-overlay" style="background-color: #eee" >

  <button class="menu-btn food-btn" data-url="food"><img width="50" src="/img/fast.png" ></button>
  <button class="menu-btn drink-btn" data-url="drinks"><img width="50" src="/img/beer.png" ></button>
  <button class="menu-btn cocktail-btn" data-url="cocktails"><img width="50" src="/img/cocktail.png" ></button>
  <div class='menu-container'>


  </div>
</div>


