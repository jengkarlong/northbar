<section id="my-placeholder-3" class="my-section my-container event">

  <h2>News &amp; Events</h2>

  <div id="events-container">
    <div class="news">
      <h3>Latest News</h3>
      @if(count($news) > 0)
        @foreach ($news as $item)
    
        <div class="news-item">
          <h4 class="news-title">{{$item->title}}</h4>
          <p class="news-description">{{$item->body}}</p>
          <a data-index="{{$item->id}}" class='read-more'>Read More</a>
        </div>
        @endforeach
      @else
        <div class="news-item" style="opacity: 0.5">
          <h4>No news to post</h4>
        </div>
      @endif
      
    </div>

    <div class="events">
      <h3>Upcoming Events</h3>
      @foreach ($events as $event)
      <div class="event-item">
          <h4 class="event-title">{{$event->title}}</h4>
          <span class="event-month">{{date("M", strtotime($event->date))}}</span>
          <span class="date" style="margin-right: 10px"><strong>Date: </strong>{{date('D M j', strtotime($event->date))}}</span>
          <span class="time"><strong>Start: </strong>{{date('g:i A' ,strtotime($event->date))}}</span>
          <a data-index="{{$event->id}}" class="view-event">View Event</a>
      </div>
      @endforeach

    </div>
  </div>
</section>

<!-- Will be called by ajax -->
<div id="news-overlay" class='no-show' >

</div>
<div id="loader">
  <div class="cssload-loader">
    <div class="cssload-top"></div>
    <div class="cssload-bottom"></div>
    <div class="cssload-line"></div>
  </div>  
</div>