<header class="my-header">
  <div id="my-logo">
    <a href="/">
      <img src="/img/white-logo.png" alt="">
    </a>
  </div>
</header>
	<section id="my-contact" style="background-image: url('/img/header.jpg')">
		<div id="my-contact-address">
			<h1>N10103 WI-49 Iola, Wisconsin</h1>
      <a href="tel: 715-677-3491" class="my-btn"><i class="fas fa-phone"> </i> 715-677-3491</a>
      <h2><a href="#">Follow us on <i class="fab fa-facebook-f"></i></a></h2>
		</div> <!-- #my-contact-address -->
	</section> <!-- #my-contact -->
	<div class="my-main-nav">
		<a href="#0" class="my-main-nav-trigger">Menu<span></span></a> <!-- button visible on small devices -->
		<nav>
			<ul>
				<li>
					<a href="#my-placeholder-1">
						<b>About</b>
						<span><i class="fas fa-info"></i></span>
					</a>
				</li>
				<li>
					<a href="#my-placeholder-2">
						<b>Menu</b>
						<span><i class="fas fa-utensils"></i></span>
					</a>
				</li>
				<li>
					<a href="#my-placeholder-3">
						<b>Events</b>
						<span><i class="fas fa-asterisk"></i></span>
					</a>
				</li>
				<li>
					<a href="#my-placeholder-4">
						<b>Find us</b>
						<span><i class="fas fa-map-marker-alt"></i></span>
					</a>
				</li>
				<li>
					<a href="#my-placeholder-5">
						<b>Get in touch</b>
						<span><i class="fas fa-comment-alt"></i></span>
					</a>
				</li>
			</ul>
		</nav>
	</div> <!-- .my-main-nav -->