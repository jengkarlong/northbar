<?php

if (env('APP_ENV') === 'production') {
    URL::forceSchema('https');
}

Route::get('/', "HomeController@index");

Route::get('/events', "EventController@index");

Route::get('/events/show', "EventController@show");

Route::get('/events/create', "EventController@create");

Route::post('/events/store', "EventController@store");

Route::get('/events/update', "EventController@modalUpdate");

Route::post('/events/update', "EventController@update");

Route::post('events/destroy', "EventController@destroy");

Route::get('/news', "NewsController@index");

Route::get('/news/show', "NewsController@show");

Route::get('/news/create', "NewsController@create");

Route::post('/news/store', "NewsController@store");

Route::get('/news/update', "NewsController@modalUpdate");

Route::post('/news/update', "NewsController@update");

Route::post('/news/destroy', "NewsController@destroy");

Route::get('/menus', "MenuController@index")->name('menu');

Route::get('/menus/show', "MenuController@show");

Route::get('/menus/create', "MenuController@create");

Route::post('/menus/store', "MenuController@store");

Route::get('/menus/update', "MenuController@modalUpdate");

Route::post('/menus/update', "MenuController@update");

Route::post('/menus/destroy', "MenuController@destroy");

Route::get('/login', "LoginController@create")->name('login');

Route::get('/logout', "LoginController@destroy");

Route::post('/login', "LoginController@store");
