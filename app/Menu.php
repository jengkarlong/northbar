<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
  protected $fillable = ['name', 'category', 'subcategory', 'price', 'description'];
}
