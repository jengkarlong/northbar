<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construc()
    {
        $this->middleware('guest')->except('destroy');

    }

    public function create()
    {
        return view('admin.login');
    }

    public function store()
    {
        if (!auth()->attempt(request(['username', 'password']))) {
            return back()->withErrors([
                'message' => 'Incorrect username or password'
            ]);
        }
        
        return redirect()->route('menu');
    }

    public function destroy()
    {

        auth()->logout();

        return redirect()->route('login');
    }
}
