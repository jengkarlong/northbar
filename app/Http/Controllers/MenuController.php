<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    public function index()
    {
        $menus = Menu::orderBy('category')
                    ->orderBy('subcategory')
                    ->orderBy('name')
                    ->get();

        return view('menus.index', compact('menus'));
    }

    public function show()
    {
        $sub = Menu::selectRaw('count(*), subcategory')
            ->groupBy('subcategory')
            ->where('category',request('menu'))
            ->get();             

        $category = request('menu');

        return view('menus.show', compact(['sub', 'category']));
    }

    public function create()
    {
        return view('menus.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'name' => 'required',
            'category' => 'required',
            'subcategory' => 'required',
            'price' => 'required',
            'description' => 'max: 150'    
        ]);

      
        $subcategory = strtolower(request('subcategory'));
        
        Menu::create([
            'name' => request('name'),
            'category' => request('category'),
            'subcategory' => $subcategory,
            'price' => request('price'),
            'description' => request('description')
        ]);
          
        return redirect()->back();
    }

    public function modalUpdate() {
       $menu = Menu::where('id', request('id'))->first();

       return view('menus.update', compact('menu'));
    }

    public function update() {
        
        $subcategory = strtolower(request('subcategory'));
        $row = Menu::where('id', request('id'))->update([
            'category' => request('category'),
            'subcategory' => $subcategory,
            'name' => request('name'),
            'price' => request('price'),
            'description' => request('description'),

            ]);

    }

    public function destroy()
    {
        Menu::where('id', request('id'))->delete();

    }
}
