<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;

class EventController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth')->except('show');
  }
  public function index()
  {
    $events = Event::all();

    return view('events.index', compact('events'));
  }

  public function show()
  {
    $id = request('id');
    $event = Event::where('id', $id)->first();

    return view('events.show', compact('event'));
  }

  public function create()
  {
    return view('events.create');
  }

  public function store()
  {
    $this->validate(request(), [
      'title' => 'required',
      'date' => 'required',
      'image' => 'required|mimes:jpeg,jpg,png,pdf,gif'
    ]);


    $fileName = User::uploadImage(request()->image, "img/events");
    
    Event::create([
      'title' => request('title'),
      'date' => request('date'),
      'image' => $fileName
    ]);
    
    return redirect()->back();
  }

  public function modalUpdate() {
    $event = Event::where('id', request('id'))->first();
    return view('events.update', compact('event'));
  }

  public function update() {
    
    $row = Event::where('id', request('id'))->update([
        'title' => request('title'),
        'date' => request('date')

        ]);

  }

  public function destroy()
  {
    $id = request('id');
    Event::where('id', $id)->delete();
    
  }
}
