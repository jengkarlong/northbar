<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Event;

class HomeController extends Controller
{
    public function index()
    {
        $news = News::orderBy('created_at','DESC')->get();
        $events = Event::orderBy('date')->get();

        return view('home.index', compact(['news', 'events']));
    }
}
