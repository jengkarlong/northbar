<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\User;

class NewsController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth')->except('show');
  }

  public function index()
  {
    $news = News::orderBy('created_at', 'DESC')->get();

    return view('news.index', compact('news'));
  }

  public function create()
  {
    return view('news.create');
  }

  public function show()
  {
    $id = request('id');
    $news = News::where('id', $id)->first();

    return view('news.show', compact('news'));
    
  }

  public function store()
  {
    $this->validate(request(), [
      'title' => 'required',
      'body' => 'required',
      'image' => 'required|mimes:jpeg,jpg,png,pdf,gif'
    ]);

    $fileName = User::uploadImage(request()->image, "img/news");
    
    News::create([
      'title' => request('title'),
      'body' => request('body'),
      'image' => $fileName
    ]);


    
    return redirect()->back();
  }

public function modalUpdate() {
    $news = News::where('id', request('id'))->first();
    return view('news.update', compact('news'));
 }

 public function update() {
     
     $row = News::where('id', request('id'))->update([
         'title' => request('title'),
         'body' => request('body')

         ]);

 }

  public function destroy()
  {
    $id = request('id');

    News::where('id', $id)->delete();
  }
}
